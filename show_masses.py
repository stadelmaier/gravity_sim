import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ma
from newton import simulate
from test_masses_list import user_list


def init_animation():

    return


def update_frame(frame, mass_list, dots):

    for i in range(len(dots)):
        d = dots[i]
        m = mass_list[i]
        pos = m._pos_history[frame]
        # print(d)
        # print([pos._X, pos._Y, pos._Z])
        d._offsets3d = ([pos._X, ], [pos._Y, ], [pos._Z, ])
        # d.set_size(2)

    return


def show_3d(mass_list, frame_number=10**3):

    fig = plt.figure(figsize=(12, 9))
    ax = plt.axes(projection="3d")

    dots = []
    for m in mass_list:
        pos = m._pos_init
        p = ax.scatter3D(pos._X, pos._Y, pos._Z, s=m._size, c=m._color)
        dots.append(p)

    ax.set_xlim(-250, 250)
    ax.set_ylim(-250, 250)
    ax.set_zlim(-250, 250)

    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])

    ani = ma.FuncAnimation(fig, update_frame, fargs=(mass_list, dots), frames=frame_number, interval=5, blit=True)
    # ani.save("simulation_out.mp4", fps=60, dpi=300)
    plt.show()

    return ani


def check_energy_conservation(mass_list):

    for m in mass_list:

        p = m._mom_history
        x = m._pos_history

        others = [mm for mm in mass_list if mm is not m]

        kin_en = np.array([0.5 * dp*dp / dt / m._mass for dp in p])
        grav_en = np.zeros(kin_en.shape)

        for other_mass in others:

            potential_hist = []
            for i in range(len(x)):

                dist = abs(x[i] - other_mass._pos_history[i])
                potential = m._mass * other_mass._mass / dist
                potential_hist.append(potential)

            grav_en -= np.array(potential_hist)

        t = np.linspace(0, time_lim, num=len(x))
        plt.plot(t, grav_en, label="pot")
        plt.plot(t, kin_en, label="kin")
        plt.plot(t, grav_en+kin_en, label="sum")
        plt.plot(t, grav_en/kin_en, label="ratio")
        plt.legend()
        plt.show()

    return


if "__main__" == __name__:

    print(user_list)

    time_lim = 2 * 10**2
    dt = 0.05

    simulate(user_list, dt=dt, time_limit=time_lim)

    # check_energy_conservation(user_list)

    print()
    print("Exporting ...")

    # [print(pos) for pos in user_list[1]._pos_history]
    # [print(force) for force in user_list[1]._force_history]

    show_3d(user_list, int(time_lim/dt))
