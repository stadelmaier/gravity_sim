import numpy as np
# import warnings


class vec:
    """ euclidean vectors """

    def __init__(self, x: float, y: float, z: float):
        self._X = x
        self._Y = y
        self._Z = z

    def __add__(self, other):
        return vec(self._X + other._X,
                   self._Y + other._Y,
                   self._Z + other._Z)

    def __iadd__(self, other):
        return self + other

    def __sub__(self, other):
        return vec(self._X - other._X,
                   self._Y - other._Y,
                   self._Z - other._Z)

    def __mul__(self, other):
        try:
            return self._X * other._X + self._Y * other._Y + self._Z * other._Z
        except AttributeError:
            # warnings.warn("Wrong order for float / vector multiplication.")
            return other * self

    def __rmul__(self, other):
        return vec(self._X * other,
                   self._Y * other,
                   self._Z * other)

    def __truediv__(self, other):
        return 1./other * self

    def __abs__(self):
        return np.sqrt(self._X**2 + self._Y**2 + self._Z**2)

    def __repr__(self):
        return f"({self._X:.2f}, {self._Y:.2f}, {self._Z:.2f})"


def counter(func, counter_dict={}):
    counter_dict[func] = 0

    def wrap(*args, **kwargs):
        num = counter_dict[func]
        counter_dict[func] += 1
        # print(kwargs)
        obj = func(*args, **kwargs, number=num)
        return obj
    return wrap


class test_mass:
    """ test mass class """

    @counter
    def __init__(self, pos: vec, momentum: vec,
                 mass=1, size=None, name=None, number=None, color="k", star_time=0):

        self._pos_init = pos
        self._mom_init = momentum

        self._pos = self._pos_init
        self._mom = self._mom_init

        self._pos_history = [self._pos_init]
        self._mom_history = [self._mom_init]

        self._mass = mass

        self._parent = None
        self._children = []

        if size is None:
            self._size = self._mass
        else:
            self._size = size

        self._number = number

        if name is None:
            self._name = "m%i" % self._number
        else:
            self._name = name

        self._color = color

        self._t = [star_time]

        self._force = [self._mom_init / self._mass]
        self._force_history = [self._mom_init / self._mass]

    def __repr__(self):
        rep_string = f"\n{self._name}, m = {self._mass}" + \
         f"\nx  = {self._pos}, p  = {self._mom}"
        return rep_string

    def move(self, f: vec, dt=1):

        if self._parent is not None:
            self._mom = self._parent._mom
            self._pos = self._parent._pos
            self._force = vec(0, 0, 0)

        if self._mass == 0:
            self._pos += self._mom
            self._force = vec(0, 0, 0)

        elif self._parent is None:
            self._mom += f * dt
            self._pos += self._mom/self._mass
            self._force = f

        self._pos_history.append(self._pos)
        self._mom_history.append(self._mom)
        self._force_history.append(self._force)
        return


def merge_masses(parent, child):
    print("Merging: %s < %s \n" % (parent._name, child._name))
    child._parent = parent
    parent._children.append(child)

    parent._mass += child._mass
    parent._mom += child._mom
    return


def newton_gravitational_force(mass1, mass2, G=1):

    distance = abs(mass1._pos - mass2._pos)
    direction = (mass2._pos - mass1._pos) / distance

    if mass1 in mass2._children or mass2 in mass1._children:
        return vec(0, 0, 0)

    elif distance < 2:
        # print(mass1._name, mass2._nae)
        print("distance close:")
        print(mass1)
        print(mass2)
        print(distance)
        print()
        if mass1._mass >= mass2._mass:
            merge_masses(mass1, mass2)
        else:
            merge_masses(mass2, mass1)
        return vec(0, 0, 0)

    else:
        f = G / distance**2 * mass1._mass * mass2._mass * direction

    return f


def calculate_total_force(mass, other_masses_list):

    f = vec(0, 0, 0)
    for m in other_masses_list:
        f += newton_gravitational_force(mass, m)

    return f


def simulate(mass_list, dt=1, time_limit=1000):

    t = 0
    while t < time_limit:

        for m in mass_list:

            f = calculate_total_force(m, [mm for mm in mass_list if m is not mm])
            m.move(f, dt=dt)

        t += dt
        print("Simulating %.2f / %.2f       " % (t, time_limit), end="\r", flush=True)

    return
