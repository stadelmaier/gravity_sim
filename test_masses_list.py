from newton import vec, test_mass
import random


user_list = [
    test_mass(vec(150, 0, 0), vec(0, 700., 0), mass=100, size=10, name="earth", color="blue"),
    test_mass(vec(0, 0, 0), vec(0, 0, 0), mass=100000, size=50, name="sun", color="darkorange"),
    test_mass(vec(150, 12, 0), vec(5, 35., 5), mass=5, size=2, name="moon", color="k"),
    # test_mass(vec(150+random.randint(-10, 10), random.randint(0, 10),
    #           random.randint(-10, 10)), vec(3, 5., 1), mass=2, size=2,  color="k"),
    # test_mass(vec(150+random.randint(-10, 10), random.randint(0, 10),
    #           random.randint(-10, 10)), vec(-3, 5., -1), mass=2, size=2,  color="k"),
    # test_mass(vec(150+random.randint(-10, 10), random.randint(0, 10),
    #           random.randint(-10, 10)), vec(-2, 5., 2), mass=2, size=2,  color="k"),
    # test_mass(vec(150+random.randint(-10, 10), random.randint(0, 10),
    #           random.randint(-10, 10)), vec(2, 5., -2), mass=2, size=2,  color="k"),
    # test_mass(vec(150+random.randint(-10, 10), random.randint(0, 10),
    #           random.randint(-10, 10)), vec(0, 5., 0), mass=2, size=2,  color="k"),
]
